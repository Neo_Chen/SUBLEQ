/* ========= *\
|| NeoSUBLEQ ||
\* ========= */

/* subleq.h: Common Header */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

typedef int32_t mem_t;
typedef int32_t pc_t;

extern mem_t *mem;
extern pc_t pc;
extern size_t memsize;

void panic(char *msg);
void vm_mainloop(mem_t *mem, pc_t pc, size_t memsize, int debug, FILE *in, FILE *out);
pc_t readcore(mem_t *mem, size_t memsize, FILE *fd);
void dumpcore(mem_t *mem, size_t memsize, FILE *fd);
