CC=	cc
CFLAGS=	-O3 -g3 -gdwarf-5 -pipe -Wall -Wextra -I. -std=c89 -pedantic -D_DEFAULT_SOURCE
VMOBJS=	subleq.o vm.o
EXEC=	subleq
.PHONY: all clean syntax countline
all:	$(EXEC)
subleq:	$(VMOBJS)
	$(CC) $(LDFLAGS) $^ $(.ALLSRC) -o subleq

clean:
	rm -rf $(VMOBJS) $(EXEC)

syntax:
	$(CC) $(CFLAGS) -fsyntax-only *.c *.h

countline:
	wc -l *.c *.h *.l
