/* ========= *\
|| NeoSUBLEQ ||
\* ========= */

/* subleq.c: Virtual Machine */

#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <subleq.h>

mem_t *mem=NULL;
pc_t pc=0;
size_t memsize=(1<<22);
int debug=0;
FILE *corefile=NULL;

void parsearg(int argc, char **argv)
{
	int opt=0;
	corefile=stdin;
	while((opt = getopt(argc, argv, "hc:ds:")) != EOF)
	{
		switch(opt)
		{
			case 'h':
				fprintf(stderr, "%s [-h] [-d] [-c corefile] [-s size]\n", argv[0]);
				exit(0);
				break;
			case 'c':
				if(strcmp("-", optarg))
				{
					if((corefile = fopen(optarg, "r")) == NULL)
					{
						perror(optarg);
						exit(8);
					}
				}
				else
					corefile=stdin;
				pc += readcore(mem + pc, memsize, corefile);
				break;
			case 'd':
				debug++;
				break;
			case 's':
				sscanf(optarg, "%lu", &memsize);
				if(memsize == 0)
					panic("?MEM=0\n");
				free(mem);
				mem = calloc(memsize, sizeof(mem_t));
				break;
			default:
				panic("?ARG\n");
				break;
		}
	}
}

int main(int argc, char **argv)
{
	setvbuf(stdout, NULL, _IONBF, 0);
	mem = calloc(memsize, sizeof(mem_t));
	parsearg(argc, argv);
	if(debug)
	{
		puts("DUMPED CORE:");
		dumpcore(mem, pc, stderr);
		puts("=== END ===");
	}
	vm_mainloop(mem, 0, memsize, debug, stdin, stdout);
	return 0;
}
