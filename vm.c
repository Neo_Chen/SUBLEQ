/* ========= *\
|| NeoSUBLEQ ||
\* ========= */

/* vm.c: Virtual Machine Backend */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <subleq.h>

void panic(char *msg)
{
	fputs(msg, stderr);
	exit(100);
}

pc_t readcore(mem_t *mem, size_t memsize, FILE *fd)
{
	size_t ptr=0;
	while(fscanf(fd, "%d", mem + (ptr++)) > 0 && ptr < memsize);
	return (pc_t)ptr - 1;
}

void dumpcore(mem_t *mem, size_t memsize, FILE *fd)
{
	size_t ptr=0;
	for(ptr=0; ptr < memsize; ptr++)
		fprintf(fd, "%d\n", mem[ptr]);
}

void vm_mainloop(mem_t *mem, pc_t startpc, size_t memsize, int debug, FILE *in, FILE *out)
{
	pc_t pc=startpc;
	mem_t a=0, b=0, c=0;
	while(pc >= 0 && pc < (pc_t)memsize)
	{
		a = mem[pc++];
		b = mem[pc++];
		c = mem[pc++];
		if(debug)
			fprintf(stderr, "PC = %d, A = %d, B = %d, C = %d\n", pc - 3, a, b, c);
		if(a < 0)
			mem[b] += (mem_t) getc(in);
		else if(b < 0)
			putc((char)mem[a], out);
		else if((mem[b] -= mem[a]) <= 0)
			pc = c;
	}
	return;
}
